from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self, first_name, last_name, email):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email

    def getFullName(self):
        return f"{self.first_name} {self.last_name}"

    @abstractmethod
    def login(self):
        pass

    @abstractmethod
    def logout(self):
        pass

class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email)
        self.department = department

    def login(self):
        return f"{self.email} has logged in"

    def addRequest(self):
        return "Request has been added"

    def logout(self):
        return f"{self.email} has logged out"

class Admin(Employee):
    def addUser(self):
        return "User has been added"

class TeamLead(Employee):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self.members = []

    def addMember(self, member):
        self.members.append(member)

    def get_members(self):
        return self.members

class Request:
    def __init__(self, description, requester, date):
        self.description = description
        self.requester = requester
        self.date = date
        self.status = "open"

    def set_status(self, status):
        self.status = status

    def closeRequest(self):
        return f"Request {self.description} has been closed"









# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())